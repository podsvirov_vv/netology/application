FROM golang:1.11-alpine AS build

WORKDIR /src/
COPY /src/main.go go.* /src/
RUN CGO_ENABLED=0 go build -o /bin/vlad

FROM scratch
COPY --from=build /bin/vlad /bin/vlad
ENTRYPOINT [ "/bin/vlad" ]
